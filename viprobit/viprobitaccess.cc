#include <node.h>
#include <bitaccess.h>
#include <bittypes.h>
#include "viprobitupdates.h"

extern int readBitNames(void);

using namespace v8;
using namespace viprobitupdate;

int ipc_fd = -1;
BitUpdates *bitUpdates = NULL;

void listenForBitUpdates(const FunctionCallbackInfo<Value> &args);
void stopBitUpdates(const FunctionCallbackInfo<Value> &args);

/***
 * getBitValueForIndex()
 * returns bit value from the bit store for the provided bit index
 * **/
void getBitValueForIndex(const FunctionCallbackInfo<Value> &args)
{
    Isolate *isolate = Isolate::GetCurrent();
    HandleScope scope(isolate);
    if (args.Length() < 1)
    {
        isolate->ThrowException(String::NewFromUtf8(isolate, "Wrong number of arguments. Bit index not specified"));
        return;
    }
    uint32_t bitVal = getBit_byIndex(args[0]->Int32Value());
    // printf("Index is %d value is %04x\n", args[0]->Int32Value(), bitVal);
    args.GetReturnValue().Set(bitVal);
}

/**
 * module init method
 * **/
void init(Handle<Object> exports)
{
    NODE_SET_METHOD(exports, "getBitValueForIndex", getBitValueForIndex);
    NODE_SET_METHOD(exports, "startUpdates", listenForBitUpdates);
    NODE_SET_METHOD(exports, "stopUpdates", stopBitUpdates);
    readBitNames();
}

/**
 * listenForBitUpdates()
 * Start listening for bit and variable updates.
 * The function expects an callback argument which will be called once an update is received
 * **/
void listenForBitUpdates(const FunctionCallbackInfo<Value> &args)
{
    Isolate *isolate = Isolate::GetCurrent();
    HandleScope scope(isolate);
    if (args.Length() < 1)
    {
        isolate->ThrowException(Exception::Error(String::NewFromUtf8(isolate, "Must be called with an argument")));
        return;
    }
    if (!args[0]->IsFunction())
    {
        isolate->ThrowException(Exception::TypeError(String::NewFromUtf8(isolate,
                                                                         "Invalid argument specified. The argument must be a callback function")));
        return;
    }
    bitUpdates = new BitUpdates(args);
    bitUpdates->startUpdates();
}

void stopBitUpdates(const FunctionCallbackInfo<Value> &args)
{
    if (bitUpdates != NULL)
    {
        bitUpdates->stopUpdates();
        delete bitUpdates;
        bitUpdates = NULL;
    }
}

/**
 * module decl
 * **/
NODE_MODULE(viprobit, init)