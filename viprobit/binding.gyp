{
    "targets" :[
        {
            "target_name" : "viprobit",
            "include_dirs": ['./'],
            "sources" : ["callbackwrapper.h","message.h", 
                        "viprobitupdates.h","callbackwrapper.cpp", 
                        "viprobitupdates.cc", "viprobitaccess.cc"],
            "libraries": ["-lbitaccess", "-lipcmsglib"]
        }
    ]
}