#ifndef _CALLBACK_WRAPPER_H_
#define _CALLBACK_WRAPPER_H_
#include <v8.h>
namespace viprobitupdate
{
class BitUpdates;

class CallbackWrapper
{
  public:
    CallbackWrapper(BitUpdates *ptr);
    ~CallbackWrapper();
    static void process_message(uint8_t *msg, int msgsize);
    static void executeThread(void *args);

  private:
    static BitUpdates *mBitUpdates;
};
} // namespace viprobitupdate
#endif //_CALLBACK_WRAPPER_H_