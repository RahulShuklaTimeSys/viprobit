#include "viprobitupdates.h"
#include <unistd.h>
#include "message.h"
#include "bittypes.h"
#include "bitaccess.h"
#include <ipcmsglib.h>

namespace viprobitupdate
{
BitUpdates* CallbackWrapper::mBitUpdates = NULL;
BitUpdates::BitUpdates(const FunctionCallbackInfo<Value> &args) : isRunning(false)
{
	Local<Function> func = Local<Function>::Cast(args[0]);
	mCb.Reset(args.GetIsolate(), func);
	mWrapper = new CallbackWrapper(this);
}

BitUpdates::~BitUpdates()
{
	isRunning = false;
	mCb.Reset();
	delete mWrapper;
	mWrapper = NULL;
}

void BitUpdates::process_message(uint8_t *msg, int msgsize)
{
	printf("[bitaccess-update]:Header     %08x\n", *((uint32_t *)(msg + header_offset)));
	printf("[bitaccess-update]:Size       %08x\n", *((uint32_t *)(msg + subsize_offset)));
	printf("[bitaccess-update]:Time       %08x\n", *((uint32_t *)(msg + time_offset)));
	printf("[bitaccess-update]:SubgroupID %08x\n", *((uint32_t *)(msg + type_offset)));
	if (!mCb.IsEmpty())
	{
		//notify update message
	}
}

void BitUpdates::startUpdates()
{
	isRunning = true;
	uv_thread_create(&worker_id, CallbackWrapper::executeThread, NULL);
}

void BitUpdates::stopUpdates()
{
	isRunning = false;
	uv_thread_join(&worker_id);
}

void BitUpdates::threadFun(void *arg)
{
	fd_set fdset;
	struct timeval tv;
	int rv;
	int fdmax;
	int ipc_fd = -1;

	while (isRunning)
	{
		while (ipc_fd == -1)
		{
			ipc_fd = ipc_connect();

			if (ipc_fd != -1)
			{
				// Register for IPC Bitstore messages
				ipc_register(ipc_fd, av2com_bitstream);
				usleep(1.0e6);
				ipc_register(ipc_fd, xv2com_bitstream);
			}
			else
			{
				usleep(1.0e6);
			}
		}

		// clear the set
		FD_ZERO(&fdset);
		FD_SET(ipc_fd, &fdset);
		fdmax = ipc_fd;

		// Check for activity with a 1ms timeout
		tv.tv_sec = 0;
		tv.tv_usec = 1000;
		rv = select(fdmax + 1, &fdset, NULL, NULL, &tv);

		if (rv == -1)
		{
			// error occurred in select()
			//syslog(LOG_ERR, "[bitaccess-update]: Select() error: %s", strerror(errno));
			//return -1;
			printf("ipc error");
			//TODO: notify error
		}
		else
		{
			if (FD_ISSET(ipc_fd, &fdset))
			{
				int ret = 0;
				//syslog(LOG_INFO, "[bitaccess-update]: ipc activity");
				printf("ipc activity");
				ret = ipc_process(ipc_fd, CallbackWrapper::process_message);

				if (ret <= 0)
				{
					ipc_fd = -1;
				}
			}
			// Pause for half a second.
			usleep(5.0e5);
		}
	}
}
} // namespace viprobitupdate