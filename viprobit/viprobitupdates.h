#ifndef _BITUPDATE_H_
#define _BITUPDATE_H_
#include <uv.h>
#include <v8.h>
#include "callbackwrapper.h"

namespace viprobitupdate
{

using namespace v8;

class BitUpdates
{
  public:
    //ctor
    BitUpdates(const FunctionCallbackInfo<Value> &args);
    //dtor
    ~BitUpdates();

    //start updates
    void startUpdates();
    void stopUpdates();
    void process_message(uint8_t *msg, int msgsize);
    void threadFun(void *arg);

  private:
    bool isRunning;
    Persistent<Function> mCb;
    CallbackWrapper *mWrapper;
    uv_thread_t worker_id;
};

} // namespace viprobitupdate

#endif //_BITUPDATE_H_