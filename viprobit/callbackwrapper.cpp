#include "callbackwrapper.h"
#include "viprobitupdates.h"

namespace viprobitupdate
{
CallbackWrapper::CallbackWrapper(BitUpdates *ptr)
{
    mBitUpdates = ptr;
}

CallbackWrapper::~CallbackWrapper()
{
    mBitUpdates = NULL;
}

void CallbackWrapper::process_message(uint8_t *msg, int msgsize)
{
    if (mBitUpdates)
    {
        mBitUpdates->process_message(msg, msgsize);
    }
}

void CallbackWrapper::executeThread(void *args)
{
    if (mBitUpdates)
    {
        mBitUpdates->threadFun(args);
    }
}
} // namespace viprobitupdate