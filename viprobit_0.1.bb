DESCRIPTION = "Node.js module for ViPro Bitaccess."
LICENSE = "CLOSED"

DEPENDS = "\
    nodejs "
SRC_URI = " \
            file://binding.gyp \
            file://message.h \
            file://callbackwrapper.h \
            file://callbackwrapper.cpp \
            file://viprobitupdates.h \
            file://viprobitupdates.cc \
            file://viprobitaccess.cc"

S = "${WORKDIR}"

do_configure() {
    #export LD="${CXX}"
    #export GYP_DEFINES="sysroot=${STAGING_DIR_HOST}"
    node-gyp --no-debug --arch=${TARGET_ARCH} --target=0.12.7 --target-platform=linux --verbose configure
}

do_compile() {
    #export LD="${CXX}"
    #export GYP_DEFINES="sysroot=${STAGING_DIR_HOST}"
    export SYSROOT=${STAGING_DIR_HOST}
    node-gyp build --no-debug --arch=${TARGET_ARCH} --target_platform=linux
}

do_install() {
    
    #define the installation directory
    #TODO : This may cause conflict while extracting the node_modules.xx file
    #create installation directory
    mkdir -p ${D}/home/www/node_modules/viprobit
    #install the node module into the destination dir
    cp ${WORKDIR}/build/Release/viprobit.node ${D}/home/www/node_modules/viprobit/
}

do_package_qa() {
    rm -rf ${D}/home/www/node_modules/viprobit/.debug
}

FILES_${PN} += "/"
